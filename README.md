# Unofficial Python 3 Taurus Core

This repository is unofficial and not supported, it's only used while the official Taurus project is moving to Python 3. 

Once Taurus will be supporting Python 3 officially, this repository will be removed.

More information about Taurus and Python 3 can be found here: https://github.com/taurus-org/taurus/pull/703
