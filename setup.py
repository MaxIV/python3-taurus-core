#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name = "python3-taurus-core",
      version = "3.4.0",
      description = "Python 3 port of the taurus core.",
      packages = find_packages(),
     )
